const http = require('http');
const uuid = require('uuid');
// const dotenv = require('dotenv');
// dotenv.config();
// const port = process.env.PORT || 3000;

const { port } = require('./config')


//console.log(uuid.v4());

http.createServer((request, response) => {
  response.setHeader("Content-Type", "text/html")
  if (request.url === '/html') {
    response.write(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
        
          </body>
        </html>`)
    response.end();
  }
  else if (request.url === '/json') {
    response.setHeader("Content-Type", "application/json")
    response.write(`{
            "slideshow": {
              "author": "Yours Truly",
              "date": "date of publication",
              "slides": [
                {
                  "title": "Wake up to WonderWidgets!",
                  "type": "all"
                },
                {
                  "items": [
                    "Why <em>WonderWidgets</em> are great",
                    "Who <em>buys</em> WonderWidgets"
                  ],
                  "title": "Overview",
                  "type": "all"
                }
              ],
              "title": "Sample Slide Show"
            }
          }`)
    response.end();
  }
  else if (request.url === '/uuid') {
    response.setHeader("Content-Type", "text/html")
    response.end(uuid.v4());
  }
  else if (request.method === 'GET' && request.url.startsWith('/status')) {
    response.setHeader("Content-Type", "text/html")
    const status = Number(request.url.split('/')[2]);
    // response.statusCode = status
    // response.end(`<h1>status code: ${status}</h1>`)

    let statusCode = http.STATUS_CODES[status];

    if (statusCode === undefined || status === undefined) {
      response.statusCode = 404;
      response.end(JSON.stringify(`Bad status request`));
    }
    else {
      response.statusCode = Number(status);
      response.end(`<h1>status code: ${status}</h1>`);
    }
  }
  else if (request.method === "GET" && request.url.startsWith('/delay')) {
    response.setHeader("Content-Type","text/html")
    let time = request.url.split('/').pop()

    time = Number(time);
    console.log("time = " + time);
    console.log(isNaN(time));

    if (time < 0 || isNaN(time)) {
      //time = 0;
      response.end(JSON.stringify("Time cannot be negative or in words"));
    }
    else {
      setTimeout(() => {
        response.end(`200': 'OK`)
      }, time * 1000);
    }



  }
  else {
    response.setHeader("Content-Type","text/html")
    response.end(`<h1>Home Page</h1>`);
  }

}).listen(port, () => {
  console.log(`${port} is running on this port`)
  console.log("Server is up and running");
})